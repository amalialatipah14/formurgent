<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Buyer extends CI_Controller
{
    public function index()
    {
        $data['title'] = 'Buyer';
        $data['muser'] = $this->db->get_where('muser', ['txtEmail' =>
        $this->session->userdata('email')])->row_array();

        $data['user'] = $this->session->userdata('user');

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar', $data);
        $this->load->view('buyer/index', $data);
        $this->load->view('templates/footer');
    }
}
