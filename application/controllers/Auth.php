<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {

        if ($this->session->userdata('txtEmail')) {
            redirect('user');
        }

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login Page';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->db->get_where('muser', ['txtEmail' => $email])->row_array();

        //jika usernya ada
        if ($user) {
            //jika usernya aktif
            if ($user['bitActive'] == 1) {
                //cek password
                if (password_verify($password, $user['txtPassword'])) {
                    var_dump($user);
                    $data = [
                        'email' => $user['txtEmail'],
                        'username' => $user['txtUsername'],
                        'roleId' => $user['roleID']
                    ];
                    // $this->session->set_userdata($data);
                    $this->session->set_userdata('user', $data);
                    redirect('dashboard');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> 
                Wrong password ! </div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> 
            This email has not been activated ! </div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> 
            Email is not registered ! </div>');
            redirect('auth');
        }
    }

    public function register()
    {
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[muser.txtEmail]', [
            'is_unique' => 'This email has already registered !'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short !'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Registration Page';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/register');
            $this->load->view('templates/auth_footer');
        } else {
            $data = [
                'txtUsername' => htmlspecialchars($this->input->post('name', true)),
                'txtEmail' => htmlspecialchars($this->input->post('email', true)),
                'txtPassword' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'roleID' => 2,
                'bitActive' => 1,
                'dtmCreated' => time()
            ];

            $this->db->insert('muser', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> 
            Congratulation! Your account has been created. Please Login </div>');
            redirect('auth');
        }
    }

    public function logout()
    {

        $this->session->unset_userdata('txtEmail');
        $this->session->unset_userdata('roleID');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> 
            You have been logged out ! </div>');
        redirect('auth');
    }
}
