<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Department extends CI_Controller
{

    public function index()
    {
        $this->load->model('accessmenu');
        $this->load->model('mdepartment');

        $session = $this->session->userdata('user');
        $roleId  = $session['roleId'];

        $sideBarMenu = $this->accessmenu->userAccessMenu($roleId);
        $data['sideBarMenu'] = $sideBarMenu;

        $data['title'] = 'User';
        $data['muser'] = $this->db->get_where('muser', ['txtEmail' => $this->session->userdata('email')])->row_array();
        $data['user']  = $session;

        $data['departments'] = $this->mdepartment->getMDept();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('master/department', $data);
        $this->load->view('templates/footer');
    }
}
