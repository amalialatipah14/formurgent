<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function index()
    {
        $this->load->model('accessmenu');
        $this->load->model('muser');

        $session = $this->session->userdata('user');
        $roleId  = $session['roleId'];

        $sideBarMenu = $this->accessmenu->userAccessMenu($roleId);
        $data['sideBarMenu'] = $sideBarMenu;

        $data['title'] = 'User';
        $data['muser'] = $this->db->get_where('muser', ['txtEmail' => $this->session->userdata('email')])->row_array();
        $data['user']  = $session;

        $data['users'] = $this->muser->getMUser();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('master/user', $data);
        $this->load->view('templates/footer');
    }
}
