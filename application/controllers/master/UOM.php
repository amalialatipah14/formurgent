<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UOM extends CI_Controller
{

    public function index()
    {
        $this->load->model('accessmenu');
        $this->load->model('muom');

        $session = $this->session->userdata('user');
        $roleId  = $session['roleId'];

        $sideBarMenu = $this->accessmenu->userAccessMenu($roleId);
        $data['sideBarMenu'] = $sideBarMenu;

        $data['title'] = 'User';
        $data['muser'] = $this->db->get_where('muser', ['txtEmail' => $this->session->userdata('email')])->row_array();
        $data['user']  = $session;

        $data['uoms'] = $this->muom->getMUom();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('master/UOM', $data);
        $this->load->view('templates/footer');
    }

    public function create()
    {
        $this->load->model('accessmenu');
        $this->load->model('muom');

        $session = $this->session->userdata('user');
        $roleId  = $session['roleId'];

        $sideBarMenu = $this->accessmenu->userAccessMenu($roleId);
        $data['sideBarMenu'] = $sideBarMenu;

        $data['title'] = 'User';
        $data['muser'] = $this->db->get_where('muser', ['txtEmail' => $this->session->userdata('email')])->row_array();
        $data['user']  = $session;

        $data['uoms'] = $this->muom->getMUom();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('master/UOM/UOMForm', $data);
        $this->load->view('templates/footer');
    }
}
