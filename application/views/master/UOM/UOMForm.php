<div class="content-body">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Add UOM</h4>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="form-group col-md-6">
                        <label>UOM Code</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-12">
                        <label>UOM Name</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-12">
                        <label>UOM Description</label>
                        <textarea type="text" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox">
                            <label class="form-check-label">
                                Status Active
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>