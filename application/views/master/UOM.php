<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Master UOM</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">UOM</a></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-primary" onclick="onAddUom()">Add UOM</button>
                        <hr>
                        <table id="example" class="display" style="min-width: 845px">
                            <thead>
                                <tr>
                                    <th>UOM ID</th>
                                    <th>UOM Code</th>
                                    <th>UOM Name</th>
                                    <th>Description</th>
                                    <th>Status Active</th>
                                    <th class="text-center"><i class="fas fa-cogs"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($uoms as $uom) : ?>
                                    <tr>
                                        <td><?= $uom->mUomID ?></td>
                                        <td><?= $uom->mUomCode ?></td>
                                        <td><?= $uom->mUomName ?></td>
                                        <td><?= $uom->mUomDescription ?></td>
                                        <td><?= $uom->mUomBitActive ?></td>
                                        <td class="text-center">
                                            <div class="btn btn-xs btn-warning">
                                                <i class="fas fa-edit"></i>
                                            </div>
                                            <div class="btn btn-xs btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const onAddUom = () => {
        window.location.href = '/formurgent/master/uom/create';
    }
</script>