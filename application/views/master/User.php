<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Master User</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">User</a></li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-primary">Add User</button>
                        <hr>
                        <div class="table-responsive">
                            <table id="example" class="display" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date of Birth</th>
                                        <th>Adress</th>
                                        <th>No HP</th>
                                        <th>Username</th>
                                        <th>Status Active</th>
                                        <th class="text-center"><i class="fas fa-cogs"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $user) : ?>
                                        <tr>
                                            <td><?= $user->intUserID ?></td>
                                            <td><?= $user->txtName ?></td>
                                            <td><?= $user->txtEmail ?></td>
                                            <td><?= $user->dtmTTL ?></td>
                                            <td><?= $user->txtAlamat ?></td>
                                            <td><?= $user->intNoHP ?></td>
                                            <td><?= $user->txtUsername ?></td>
                                            <td><?= $user->bitActive ?></td>
                                            <td class="text-center">
                                                <div class="btn btn-xs btn-warning">
                                                    <i class="fas fa-edit"></i>
                                                </div>
                                                <div class="btn btn-xs btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>