<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4><?php echo date('l, j/m/Y'); ?></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">User</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="ti-user text-dark border-dark"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Daily Request</div>
                            <div class="stat-digit">12</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="ti-user text-dark border-dark"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Monthly Request</div>
                            <div class="stat-digit">220</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="ti-user text-dark border-dark"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Yearly Request</div>
                            <div class="stat-digit">1350</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Reason</h4>
                    </div>
                    <div class="card-body">
                        <div class="ct-bar-chart mt-5"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Most Request</h4>
                    </div>
                    <div class="card-body">
                        <div class="ct-pie-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->