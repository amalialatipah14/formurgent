<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>List Request</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">List Request</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="display" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Name Item/Service</th>
                                        <th>Status</th>
                                        <th>Date Required</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Cuci Piring</td>
                                        <td>
                                            <span class="badge badge-success">Approve</span>
                                        </td>
                                        <td>2022/01/01</td>
                                        <td class="text-center">
                                            <div class="btn btn-xs btn-warning">
                                                <i class="fas fa-edit"></i>
                                            </div>
                                            <div class="btn btn-xs btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Setrika</td>
                                        <td>
                                            <span class="badge badge-warning">Need Approve</span>
                                        </td>
                                        <td>2022/01/01</td>
                                        <td class="text-center">
                                            <div class="btn btn-xs btn-warning">
                                                <i class="fas fa-edit"></i>
                                            </div>
                                            <div class="btn btn-xs btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Tisu</td>
                                        <td>
                                            <span class="badge badge-danger">Not Approve</span>
                                        </td>
                                        <td>2022/01/01</td>
                                        <td class="text-center">
                                            <div class="btn btn-xs btn-warning">
                                                <i class="fas fa-edit"></i>
                                            </div>
                                            <div class="btn btn-xs btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-16">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Request</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Barang</th>
                                    <th>Status</th>
                                    <th>Date Required</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>

<!--**********************************
            Content body end
        ***********************************-->