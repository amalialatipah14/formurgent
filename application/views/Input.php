<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="header-left">
                    <div class="search_bar">
                        <span class="search_icon p-3 c-pointer">
                            <i class="mdi mdi-magnify"></i>
                        </span>
                        <div class="search_bar p-0 m-0">
                            <form>
                                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Data Input</a></li>
                </ol>
            </div>
        </div>
        <div class="col-lg-16">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data Input</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No PR</th>
                                    <th>Item</th>
                                    <th>Jumlah</th>
                                    <th>UOM</th>
                                    <th>Deskripsi</th>
                                    <th>Tgl Kebutuhan</th>
                                </tr>
                            </thead>
                            <th>
                                <div class="form-check mb-2">
                                    <input type="checkbox" class="form-check-input" id="check1" value="">
                                    <label class="form-check-label" for="check1"></label>
                                </div>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()" data-toggle="tooltip" data-placement="top" title="Close"><i class="fa fa-close color-danger"></i></a></span>
                            </td>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-20 col-form-">
                    <button type="submit" class="btn btn-primary">Input</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--**********************************
            Content body end
        ***********************************-->