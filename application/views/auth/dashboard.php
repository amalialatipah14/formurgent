<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>User</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/'); ?>./images/kalbe.png">
    <link href="<?= base_url('assets/'); ?>./vendor/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./vendor/chartist/css/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./css/style.css" rel="stylesheet">

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="search_bar dropdown">
                                <div class="welcome-text">
                                    <h3>FORM PEMBELIAN URGENT</h3>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-20 col-form-label">
                                <button type="submit" class="btn btn-primary" redirect>Login</button>
                            </div>
                        </div>
                        </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="row page-titles mx-0">
            <div class="col-center">
                <div class="welcome-text text-center">
                    <h3>FORM PEMBELIAN URGENT</h3>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="ti-user text-dark border-dark"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Daily Request</div>
                            <div class="stat-digit">12</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="ti-user text-dark border-dark"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Monthly Request</div>
                            <div class="stat-digit">220</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="ti-user text-dark border-dark"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Yearly Request</div>
                            <div class="stat-digit">1350</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Reason</h4>
                    </div>
                    <div class="card-body">
                        <div class="ct-bar-chart mt-5"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Most Request</h4>
                    </div>
                    <div class="card-body">
                        <div class="ct-pie-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>