<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Dashboard</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/'); ?>./images/kalbe.png">
    <link href="<?= base_url('assets/'); ?>./vendor/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./vendor/chartist/css/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./css/style.css" rel="stylesheet">

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="<?= base_url('assets/'); ?>" class="brand-logo">
                <img class="logo-abbr" src="./images/kalbe.png" alt="">
                <img class="logo-compact" src="./images/kalbe.png" alt="">
                <img class="brand-title" src="./images/kalbe.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="col">
                            <div class="welcome-text text-center">
                                <h3>FORM PEMBELIAN URGENT</h3>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="quixnav">
            <div class="quixnav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label first">BUYER</li>
                    <!-- <li><a href="index.html"><i class="icon icon-single-04"></i><span class="nav-text">Dashboard</span></a>
                    </li> -->
                    <li><a href="<?= base_url('assets/'); ?>dashboard.html" aria-expanded="false">
                            <i class="icon icon-single-04"></i>
                            <span class="nav-text">Dashboard</span></a></li>

                    <li class="nav-label first">MAIN MENU</li>
                    <li><a href="<?= base_url('assets/'); ?>dashboard.html" aria-expanded="false">
                            <i class="icon icon-single-copy-06"></i>
                            <span class="nav-text">Data Input</span></a></li>

                    <li><a href="<?= base_url('assets/'); ?>dashboard.html" aria-expanded="false">
                            <i class="icon icon-form"></i>
                            <span class="nav-text">List Request</span></a></li>

                    <li><a href="<?= base_url('assets/'); ?>dashboard.html" aria-expanded="false">
                            <i class="icon icon-chart-bar-33"></i>
                            <span class="nav-text">Report</span></a></li>

                    <li><a href="<?= base_url('assets/'); ?>dashboard.html" aria-expanded="false">
                            <i class="icon-key"></i>
                            <span class="nav-text">Logout</span></a></li>
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <div class="header-left">
                            <div class="search_bar">
                                <span class="search_icon p-3 c-pointer">
                                    <i class="mdi mdi-magnify"></i>
                                </span>
                                <div class="search_bar p-0 m-0">
                                    <form>
                                        <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Buyer</a></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-7">
                        <div class="card">
                            <div class="stat-widget-one card-body">
                                <div class="stat-icon d-inline-block">
                                    <i class="ti-user text-dark border-dark"></i>
                                </div>
                                <div class="stat-content d-inline-block">
                                    <div class="stat-text">Daily Request</div>
                                    <div class="stat-digit">12</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-7">
                        <div class="card">
                            <div class="stat-widget-one card-body">
                                <div class="stat-icon d-inline-block">
                                    <i class="ti-user text-dark border-dark"></i>
                                </div>
                                <div class="stat-content d-inline-block">
                                    <div class="stat-text">Monthly Request</div>
                                    <div class="stat-digit">220</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-7">
                        <div class="card">
                            <div class="stat-widget-one card-body">
                                <div class="stat-icon d-inline-block">
                                    <i class="ti-user text-dark border-dark"></i>
                                </div>
                                <div class="stat-content d-inline-block">
                                    <div class="stat-text">Yearly Request</div>
                                    <div class="stat-digit">1350</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Reason</h4>
                            </div>
                            <div class="card-body">
                                <div class="ct-bar-chart mt-5"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Most Request</h4>
                            </div>
                            <div class="card-body">
                                <div class="ct-pie-chart"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--**********************************
            Content body end
        ***********************************-->


    <!--**********************************
            Footer start
        ***********************************-->
    <div class="footer">
        <div class="copyright">
            <p>Copyright © <a href="<?= base_url('assets/'); ?>#" target="_blank">PT. Kalbe Morinaga Indonesia - 2021</a></p>
        </div>
    </div>
    <!--**********************************
            Footer end
        ***********************************-->

    <!--**********************************
           Support ticket button start
        ***********************************-->

    <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="<?= base_url('assets/'); ?>./vendor/global/global.min.js"></script>
    <script src="<?= base_url('assets/'); ?>./js/quixnav-init.js"></script>
    <script src="<?= base_url('assets/'); ?>./js/custom.min.js"></script>

    <script src="<?= base_url('assets/'); ?>./vendor/chartist/js/chartist.min.js"></script>

    <script src="<?= base_url('assets/'); ?>./vendor/moment/moment.min.js"></script>
    <script src="<?= base_url('assets/'); ?>./vendor/pg-calendar/js/pignose.calendar.min.js"></script>


    <script src="<?= base_url('assets/'); ?>./js/dashboard/dashboard-2.js"></script>
    <!-- Circle progress -->

</body>

</html>