<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Registrasi</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <link href="./css/style.css" rel="stylesheet">

</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <form class="MUser" method="post" action="<?= base_url('auth/register'); ?> ">
                                    <div class="auth-form">
                                        <h3 class="text-center mb-4">REGISTER</h3>
                                        <form action="index.html">
                                            <div class="form-group">
                                                <label><strong>Username</strong></label>
                                                <input type="text" id="name" name="name" class="form-control" placeholder="Full Name" value="<?= set_value('name'); ?>">
                                                <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="form-group">
                                                <label><strong>Email</strong></label>
                                                <input type="text" id="email" name="email" class="form-control" placeholder="Email Address" value="<?= set_value('email'); ?>">
                                                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <label><strong>Password</strong></label>
                                                    <input type="password" id="password1" name="password1" class="form-control" placeholder="Must Have 8 Character">
                                                    <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                                                </div>
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <label><strong>Repeat Password</strong></label>
                                                    <input type="password" id="password2" name="password2" class="form-control" placeholder="Repeat">
                                                </div>
                                            </div>
                                            <div class="text-center mt-4">
                                                <button type="submit" class="btn btn-primary btn-block">Register Account</button>
                                            </div>
                                        </form>
                                        <div class="new-account mt-3">
                                            <p>Already have an account? <a class="text-primary" href="<?= base_url(); ?>auth">Sign in</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="./vendor/global/global.min.js"></script>
    <script src="./js/quixnav-init.js"></script>
    <!--endRemoveIf(production)-->
</body>

</html>