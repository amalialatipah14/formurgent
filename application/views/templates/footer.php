<!--**********************************
            Footer start
        ***********************************-->
<div class="footer">
    <div class="copyright">
        <p>Copyright © <a href="<?= base_url('assets/'); ?>#" target="_blank">PT. Kalbe Morinaga Indonesia <?= date('Y') ?></a></p>
    </div>
</div>
<!--**********************************
            Footer end
        ***********************************-->

<!--**********************************
           Support ticket button start
        ***********************************-->

<!--**********************************
           Support ticket button end
        ***********************************-->


</div>
<!--**********************************
        Main wrapper end
    ***********************************-->

<!--**********************************
        Scripts
    ***********************************-->
<!-- Required vendors -->
<script src="<?= base_url('assets/'); ?>./vendor/global/global.min.js"></script>
<script src="<?= base_url('assets/'); ?>./js/quixnav-init.js"></script>
<script src="<?= base_url('assets/'); ?>./js/custom.min.js"></script>

<script src="<?= base_url('assets/'); ?>./vendor/chartist/js/chartist.min.js"></script>

<script src="<?= base_url('assets/'); ?>./vendor/moment/moment.min.js"></script>
<script src="<?= base_url('assets/'); ?>./vendor/pg-calendar/js/pignose.calendar.min.js"></script>


<script src="<?= base_url('assets/'); ?>./js/dashboard/dashboard-2.js"></script>
<!-- Circle progress -->
<script src="<?= base_url('assets/'); ?>./vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>./js/plugins-init/datatables.init.js"></script>

</body>

</html>