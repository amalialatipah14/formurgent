<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>User Form Urgent</title>
    <!-- Favicon icon -->
    <link href="<?= base_url('assets/'); ?>./vendor/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/'); ?>./images/kalbe.png">
    <link href="<?= base_url('assets/'); ?>./vendor/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./vendor/chartist/css/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./css/style.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="<?= base_url('assets/'); ?>">
                <img style="width: 100%; height: 100%;" src="<?= base_url('assets/'); ?>images/kalbe.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="col">
                            <div class="welcome-text text-center">
                                <h3>FORM PEMBELIAN URGENT</h3>
                            </div>
                        </div>

                    </div>
                    <ul class="navbar-nav header-right">
                        <li class="nav-item dropdown header-profile">
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                                <i class="mdi mdi-account"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item">
                                    <i class="fas fa-user"></i>
                                    <span class="ml-2"><?= $user['username'] ?></span>
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="fas fa-unlock-alt"></i>
                                    <span class="ml-2">Change Password</span>
                                </a>
                                <a href="<?= base_url('auth/logout'); ?>" class="dropdown-item">
                                    <i class="fas fa-key"></i>
                                    <span class="ml-2">Logout </span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->