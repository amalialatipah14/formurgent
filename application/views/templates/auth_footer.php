<!--**********************************
            Footer start
        ***********************************-->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <p>Copyright © <a href="<?= base_url('assets/'); ?>#" target="_blank">PT. Kalbe Morinaga Indonesia <?= date('Y') ?></a></p>
        </div>
    </div>
</footer>
<!--**********************************
            Footer end
        ***********************************-->

</div>
<!--**********************************
        Main wrapper end
    ***********************************-->

<!--**********************************
        Scripts
    ***********************************-->
<!-- Required vendors -->
<script src="<?= base_url('assets/'); ?>./vendor/global/global.min.js"></script>
<script src="<?= base_url('assets/'); ?>./js/quixnav-init.js"></script>
<script src="<?= base_url('assets/'); ?>./js/custom.min.js"></script>

<script src="<?= base_url('assets/'); ?>./vendor/chartist/js/chartist.min.js"></script>

<script src="<?= base_url('assets/'); ?>./vendor/moment/moment.min.js"></script>
<script src="<?= base_url('assets/'); ?>./vendor/pg-calendar/js/pignose.calendar.min.js"></script>


<script src="<?= base_url('assets/'); ?>./js/dashboard/dashboard-2.js"></script>
<!-- Circle progress -->

</body>

</html>