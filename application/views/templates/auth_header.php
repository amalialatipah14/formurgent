<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>
        <?= $title ?>
    </title>
    <!-- Favicon icon -->
    <link href="<?= base_url('assets/'); ?>./vendor/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/kalbe.png">
    <link href="<?= base_url('assets/'); ?>./vendor/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./vendor/chartist/css/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/'); ?>./css/style.css" rel="stylesheet">

</head>