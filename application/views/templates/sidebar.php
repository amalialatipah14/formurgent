<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">

            <?= $sideBarMenu ?>
            <!-- <li>
                <a href="<?= base_url('assets/'); ?>dashboard.html" aria-expanded="false">
                    <i class="fas fa-tachometer-alt"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
            </li>

            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="fas fa-database"></i>
                    <span class="nav-text">Master Data</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="<?= base_url('assets/'); ?>/data-department.html">Data Department</a></li>
                    <li><a href="<?= base_url('assets/'); ?>/data-user.html">Data User</a></li>
                    <li><a href="<?= base_url('assets/'); ?>/data-uom.html">Data UOM</a></li>
                </ul>
            </li> -->

            <li><a href="<?= base_url('auth/logout'); ?>" aria-expanded="false">
                    <i class="fas fa-sign-out-alt"></i>
                    <span class="nav-text">Logout</span></a></li>

            <!-- <div class="col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Sweet Image Message</h4>
                        <div class="card-content">
                            <div class="sweetalert mt-5">
                                <button class="btn btn-info btn sweet-image-message">Sweet Image
                                    Message</button>
                            </div>
                        </div>
                    </div>
                </div>
                /# card
            </div> -->

        </ul>
    </div>
</div>
<!--**********************************
            Sidebar end
        ***********************************-->