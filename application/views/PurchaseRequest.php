<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Purchase Request</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Purchase Request</a></li>
                </ol>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Request Name</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-control">Department</label>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                        <option selected="">Choose...</option>
                                        <option value="1">BDA</option>
                                        <option value="2">PRD</option>
                                        <option value="3">WHS</option>
                                        <option value="4">QA</option>
                                        <option value="5">ENG</option>
                                        <option value="6">MDP</option>
                                        <option value="7">IOS</option>
                                        <option value="8">HC</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No PR/WO</label>
                            <div class="col-sm-10">
                                <input type="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Date Created</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" data-dtp="dtp_LhEdB">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Date Required</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" data-dtp="dtp_LhEdB">
                            </div>
                        </div>
                        <a class="tombol" href="#">+ Tambah Data Baru</a>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered verticle-middle table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Item/Service</th>
                                            <th scope="col">QTY</th>
                                            <th scope="col">UOM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-control">Reason</label>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                        <option selected="">Choose...</option>
                                        <option value="1">Breakdown</option>
                                        <option value="2">Jadwal Produksi</option>
                                        <option value="3">Human Error - Miss Planning</option>
                                        <option value="4">Safety K3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2">Input Image</label>
                            <div class="basic-form custom_file_input">
                                <form action="">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form-group">
                    <div class="col-sm-20 col-form">
                        <button type="submit" class="btn btn-primary">Request</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>