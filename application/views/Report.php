<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Report</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Report</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <div class="card">
                    <div class="stat-widget-one card-body bg-success">
                        <div class="stat-content d-inline-block">
                            <div class="stat-text text-light">Approve</div>
                            <div class="stat-digit">0</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body bg-warning">
                        <div class="stat-content d-inline-block">
                            <div class="stat-text text-light">Need Approve</div>
                            <div class="stat-digit">0</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-7">
                <div class="card">
                    <div class="stat-widget-one card-body bg-danger">
                        <div class="stat-content d-inline-block">
                            <div class="stat-text text-light">Not Approve</div>
                            <div class="stat-digit">0</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
            Content body end
        ***********************************-->