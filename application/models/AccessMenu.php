<?php

class AccessMenu extends CI_Model
{
    function hasChild($parent_id)
    {
        $sql = $this->db->query("SELECT COUNT(*) as count FROM subfeatures WHERE featureId = '" . $parent_id . "' AND subFeatureId = 1")->row();
        return $sql->count;
    }

    public function userAccessMenu($roleId)
    {
        $this->db->join('features', 'useraccessmenu.featureId = features.featureId');
        $this->db->where('featureActive', 1);
        $this->db->where('roleId', $roleId);
        $query = $this->db->get('useraccessmenu');
        $list = "";
        $parent = $query->result();
        $mainList = '';

        foreach ($parent as $pr) {
            $mainList .= $this->CategoryTree($list, $pr->featureId, $pr, $append = 0);
        }

        $list .= "</li>";
        return $mainList;
    }

    function CategoryTree($list, $id, $data, $append)
    {
        if ($this->hasChild($id)) {
            $list = '<li>
                        <a class="has-arrow" href="javascript:void()" href="#" aria-expanded="false">
                        <i class="' . $data->featureIcon . '"></i><span class="nav-text">' . $data->featureName . '</span></a>';
        } else {
            $list = '<li>
                        <a href="' . $data->featureUrl . '" aria-expanded="false">
                        <i class="' . $data->featureIcon . '"></i><span class="nav-text">' . $data->featureName . '</span></a>';
        }

        if ($this->hasChild($id)) // check if the id has a child
        {
            $append++;
            $list .= '<ul aria-expanded="false">';
            // $this->db->join('features', 'useraccessmenu.featureId = features.featureId');
            $this->db->where('featureId', $id);
            $this->db->where('subFeatureActive', 1);
            $query = $this->db->get('subfeatures');
            $children = $query->result();

            foreach ($children as $key => $child) {
                $list .= "<li><a href='{$child->subFeatureUrl}'>{$child->subFeatureName}</a></li>";
            }
            $list .= "</ul>";
        }
        return $list;
    }
}
